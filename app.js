const express = require('express');

const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(express.json());

app.get("/", (req, res, next) =>{
    res.send("HELLO FALABELLA");
    next();
})

app.get("/get_meta_data/:moduleName/:screenName", (req, res, next) => {
    let moduleName = req.params.moduleName;
    let screenName = req.params.screenName
    screenName = screenName + ".json";
    res.sendFile(path.resolve(path.join(__dirname, "src/ui-config/specification"), moduleName, screenName))
});

app.listen(5000, ()=>{
    console.log(`Server started`);
})

